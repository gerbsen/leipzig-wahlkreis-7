const config = {
    wahlkreise : {
        "Wahlkreis 27 - Leipzig 1": {
            "Ortsteile": ["Anger-Crottendorf", "Althen-Kleinpösna", "Baalsdorf", "Engelsdorf", "Heiterblick",      "Holzhausen", "Mölkau", "Sellerhausen-Stünz", "Stötteritz", "Paunsdorf"],
            "borderColor": "#845EC2"
        },
        "Wahlkreis 28 - Leipzig 2": {
            "Ortsteile": ["Probstheida", "Connewitz", "Dölitz-Dösen", "Liebertwolkwitz", "Lößnig", "Marienbrunn", "Meusdorf", "Südvorstadt"],
            "borderColor": "#D65DB1"
        },
        "Wahlkreis 29 - Leipzig 3" : {
            "Ortsteile": ["Großzschocher", "Burghausen-Rückmarsdorf", "Grünau-Mitte", "Grünau-Nord", "Grünau-Ost", "Grünau-Siedlung", "Hartmannsdorf-Knautnaundorf", "Kleinzschocher", "Knautkleeberg-Knauthain", "Lausen-Grünau", "Miltitz", "Schönau"],
            "borderColor": "#FF6F91"
        },
        "Wahlkreis 30 - Leipzig 4" : {
            "Ortsteile": ["Altlindenau", "Böhlitz-Ehrenberg", "Leutzsch", "Lindenau", "Lützschena-Stahmeln", "Neulindenau", "Plagwitz", "Schleußig"],
            "borderColor": "#FF9671"
        },
        "Wahlkreis 31 - Leipzig 5" : {
            "Ortsteile": ["Reudnitz-Thonberg", "Zentrum", "Zentrum-Nord", "Zentrum-Nordwest", "Zentrum-Ost", "Zentrum-Süd", "Zentrum-Südost", "Zentrum-West"],
            "borderColor": "#FFC75F"
        },
        "Wahlkreis 32 - Leipzig 6" : {
            "Ortsteile": ["Eutritzsch", "Gohlis-Mitte", "Gohlis-Nord", "Gohlis-Süd", "Lindenthal", "Möckern", "Wahren"],
            "borderColor": "#F9F871"
        },
        "Wahlkreis 33 - Leipzig 7" : {
            "Ortsteile": ["Mockau-Nord", "Mockau-Süd", "Neustadt-Neuschönefeld", "Plaußig-Portitz", "Schönefeld-Abtnaundorf", "Schönefeld-Ost", "Seehausen", "Thekla", "Volkmarsdorf", "Wiederitzsch"],
            "borderColor": "#9BDE7E"
        },
        "Stadt Leipzig" : {
            "Ortsteile": ["Stadt Leipzig"],
            "borderColor": ""
        }
    }
};

