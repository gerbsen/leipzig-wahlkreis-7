function insertLineChart(wahlkreisNames, property, htmlDivId) {

    var ctx = document.getElementById(htmlDivId).getContext('2d');

    new Chart(ctx, {
        "type": "line",
        "data": {
            "labels": _.keys(leipzigWahlkreise[wahlkreise[0]][property]).sort(),
            "datasets": wahlkreise.map(wahlkreis => {
                return {
                    "label": wahlkreis,
                    "data": _.keys(leipzigWahlkreise[wahlkreis][property]).sort().map(key => {
                        return leipzigWahlkreise[wahlkreis][property][key];
                    }),
                    "fill": false,
                    "borderColor": config.wahlkreise[wahlkreis].borderColor,
                    "lineTension": 0.1
                }
            })
        },
        "options": {}
    });
}

