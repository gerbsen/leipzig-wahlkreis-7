# Leipzig Wahlkreis 7

This project is supposed to be the groundwork of an article which is going to be very similar to [this article](https://einundleipzig.de/gentrifizierung-in-leipzig/) bei einundleipzig.
We are using the [data](https://opendata.leipzig.de/dataset?q=kleinr%C3%A4umig&res_format=CSV&sort=score+desc%2C+metadata_modified+desc) which is provided by the Open Data Portal of the city of Leipzig.
All the data is stored in individual csv/json files on the portal.
In a first step we are merging all values of all files, group by the `Gebiet` (electorate) and the `Sachmerkmal` (feature) so that we can easily access and compare everything with everything.

## Run
```
cd $PROJECT-ROOT
node scripts/merge-open-data-portal-data.js
```

## Install

```
npm install
```

## Deploy
Commits to master are triggering a pipeline and build a docker image.
On my private server there is watchtower and this container deployed, so every new commit is immediately deploy live under:

```
    https://leipzig7.leipzig-transparent.de/
```

# Further information
- The "Wahlkreiseinteilung zur Landtagswahl 2019 in Leipzig" can be found [here](https://www.leipzig.de/buergerservice-und-verwaltung/wahlen-in-leipzig/landtagswahlen/landtagswahl-2014/wahlkreiseinteilung/).
- Inspiration from Berlin for [analysis by bus station](https://interaktiv.morgenpost.de/m29/)
- we use [Chart.js](https://www.chartjs.org/docs/latest/) so thank you for this library :)

## Stadtteile
- [Neustadt-Neuschönefeld](https://www.wikiwand.com/de/Neustadt-Neusch%C3%B6nefeld)
- [Volkmarsdorf](https://www.wikiwand.com/de/Volkmarsdorf)
- [Schönefeld-Abtnaundorf](https://www.wikiwand.com/de/Sch%C3%B6nefeld_(Leipzig))
- [Schönefeld-Ost](https://www.wikiwand.com/de/Sch%C3%B6nefeld_(Leipzig))
- [Mockau-Süd](https://www.wikiwand.com/de/Mockau)
- [Mockau-Nord](https://www.wikiwand.com/de/Mockau)
- [Thekla](https://www.wikiwand.com/de/Thekla_(Leipzig))
- [Plaußig](https://www.wikiwand.com/de/Plau%C3%9Fig)-[Portitz](https://www.wikiwand.com/de/Portitz)
- [Wiederitzsch](https://www.wikiwand.com/de/Wiederitzsch)
- [Seehausen](https://www.wikiwand.com/de/Seehausen_(Leipzig))

## Stadtbezirksbeirat

### Stadtbezirksbeirat Ost
**Adresse** Eisenbahnstraße 49, 04315 Leipzig
**Raum** IC-E Informationszentrum

- [Mitglieder](https://ratsinfo.leipzig.de/bi/au020.asp?AULFDNR=2295)
- [Sitzungen](https://ratsinfo.leipzig.de/bi/si018_a.asp?GRA=2295)

| Name|Art der Mitarbeit|herkunft|seit |
|---|---|---|---|
| Seufert, Heiner|Stadtbezirksbeiratsvorsitzende/-r|Verwaltung|17.04.2012 |
| Gerbsch, Elisa|Stadtbezirksbeirat|DIE LINKE|22.11.2018 |
| Heldt, Silvio|Stadtbezirksbeirat|CDU|18.10.2017 |
| Hufenreuter, Henry|Stadtbezirksbeirat|CDU|14.12.2011 |
| Dr. Krause, Armin|Stadtbezirksbeirat|DIE LINKE|25.03.2015 |
| Lehmann, Rocco|Stadtbezirksbeirat|Bündnis 90/Die Grünen|25.03.2015 |
| Müller, Monika|Stadtbezirksbeirat|DIE LINKE|16.12.2009 |
| Pohl, Thomas|Stadtbezirksbeirat|SPD|25.03.2015 |
| Schmidt, Kerstin|Stadtbezirksbeirat|CDU|25.03.2015 |
| Dr. Tippach, Lothar|Stadtbezirksbeirat|DIE LINKE|22.06.2016 |
| Dr. med. Weidnitzer, Sabine|Stadtbezirksbeirat|AfD|25.03.2015 |
| Weißgerber, Kerstin|Stadtbezirksbeirat|SPD|16.12.2009 |

### Stadtbezirksbeirat Nordost
**Adresse** Ossietzkystraße 37, 04347 Leipzig
**Raum** Rathaus Schönefeld, Raum 100

- [Mitglieder](https://ratsinfo.leipzig.de/bi/au020.asp?AULFDNR=2284)
- [Sitzungen](https://ratsinfo.leipzig.de/bi/si018_a.asp?GRA=2284)

| Name|Art der Mitarbeit|herkunft|seit |
|---|---|---|---|
 |Biermeier, Christel|Stadtbezirksbeiratsvorsitzende/-r||16.12.2009
 |Bittner, Uwe|Stadtbezirksbeirat|CDU|25.03.2015
 |Hofmann, Erik|Stadtbezirksbeirat|CDU|16.12.2009
 |Krause, Antje|Stadtbezirksbeirat|CDU|25.03.2015
 |Pielok, Franz|Stadtbezirksbeirat|CDU|25.03.2015
 |Eggers, Boris|Stadtbezirksbeirat|DIE LINKE|25.03.2015
 |Ertel, Petra|Stadtbezirksbeirat|DIE LINKE|26.03.2015
 |Reuther, Steffi|Stadtbezirksbeirat|DIE LINKE|16.12.2009
 |Mekschrat, Kai|Stadtbezirksbeirat|SPD|21.11.2014
 |Schirdewahn, Barbara|Stadtbezirksbeirat|SPD|16.12.2009
 |Nobbe-Fietz, Sabine|Stadtbezirksbeirat|Bündnis 90/Die Grünen|21.01.2016
 |Mehlhorn, Jens|Stadtbezirksbeirat|AfD|25.03.2015

## Ortschaftsräte

### Plaußig
**Adresse** Plaußiger Dorfstraße 23, 04349 Leipzig
**Raum** Naturschutzstation Plaußig, Schulungsraum

- [Mitglieder](https://ratsinfo.leipzig.de/bi/au020.asp?AULFDNR=2377)
- [Sitzungen](https://ratsinfo.leipzig.de/bi/si018_a.asp?GRA=2377)

| Name|Art der Mitarbeit|herkunft|seit |
|---|---|---|---|
|Richter, Ines|Ortsvorsteher/-in|FFW-P|01.01.2018
|Schwarz, Susann|Ortschaftsrat||02.09.2014
|Heberlein, Andrea|Ortschaftsrat|FFW-P|08.09.2009
|Richwien, Christian|Ortschaftsrat|CDU|01.01.2018
|Rosenkranz, Uwe|Ortschaftsrat||02.09.2014

### Seehausen
**Adresse** Am Anger 42, 04356 Leipzig
**Raum** Gasthof Hohenheida

- [Mitglieder](https://ratsinfo.leipzig.de/bi/au020.asp?AULFDNR=2312)
- [Sitzungen](https://ratsinfo.leipzig.de/bi/si018_a.asp?GRA=2312)

| Name|Art der Mitarbeit|herkunft|seit |
|---|---|---|---|
|Böhlau, Berndt|Ortsvorsteher/-in|Freie Wählergemeinschaft Seehausen|01.01.2018|
|Köckeritz, Holger|Ortschaftsrat|Freie Wählergemeinschaft Seehausen|04.08.2009|
|Prautzsch, Steffen|Ortschaftsrat||09.09.2014|
|Stannek, Detlef|Ortschaftsrat|Freie Wählergemeinschaft Seehausen|10.01.2012|
|Trantau, Olaf|Ortschaftsrat|Freie Wählergemeinschaft Seehausen|04.08.2009

## Available information

### Bautätigkeit und Wohnen

- Baufertigstellungen
  - Gebäude
  - Wohngebäude
  - Neubau von Wohngebäuden
  - Wohnungen
  - Neubau von Wohnungen
- Baugenehmigungen
  - Gebäude
  - Wohngebäude
  - Neubau von Wohngebäuden
  - Wohnungen
  - Neubau von Wohnungen
- Wohnungsbestand
  - Wohnungen
  - Wohnfläche insgesamt
  - Wohnfläche je Wohnung
- Gebäudebestand
  - Gebäude mit Wohnungen insgesamt
  - Gebäude mit 1 oder 2 Wohnungen
  - mit Baujahr bis 1918
  - mit Baujahr bis 1919 bis 1948
  - mit Baujahr bis 1949 bis 1990
  - mit Baujahr ab 1991

### Bevölkerungsbestand
- Einwohner nach Alter
  - Jugendquote
  - Altenquote
  - Durchschnittsalter
- Einwohner mit Migrationshintergund
  - Migranten
  - Ausländer
  - Deutsche mit Migrationshintergrund
  - Mehrstaater
  - Spätaussiedler
  - Migrantenanteil
  - Ausländeranteil
  - EU-Ausländer
- Einwohner nach Familienstand
  - Ledig
  - Verheiratet
  - Geschieden
  - Verwitwet
- Einwohnerdichte
  - Einwohnerdichte
- Einwohner
  - Einwohner insgesamt
  - Männer
  - Frauen
  - Deutsche
  - Ausländer
- Wohnberechtigte Einwohner nach Familienstand
  - Ledig
  - Verheiratet
  - Geschieden
  - Verwitwet
- Personenhaushalte
  - Haushalte insgesamt
  - Haushalte mit 1 Person
  - Haushalte mit 2 Personen
  - Haushalte mit 3 Personen
  - Haushalte mit 4 Personen
  - Haushalte mit 5 oder mehr Personen
  - Durchschnittliche Haushaltsgröße
  - Haushalte mit 1 Person (Anteil in %)
  - Haushalte mit 2 Personen (Anteil in %)
  - Haushalte mit 3 Personen (Anteil in %)
  - Haushalte mit 4 Personen (Anteil in %)
  - Haushalte mit 5 oder mehr Personen (Anteil in %)
- Wohnberechtigte Einwohner nach Alter
  - Durchschnittsalter
  - Jugendquote
  - Altenquote
- Wohnberechtigte Einwohner
  - Einwohner insgesamt
  - Männer
  - Frauen
  - Deutsche
  - Ausländer
### Bevölkerungsbewegung
- Geborene und Gestorbene
  - Lebendgeborene
  - Gestorbene
  - Saldo Geburten/Sterbefälle
- Wanderungen
  - Zuzüge über die Stadtgrenze
  - Wegzüge über die Stadtgrenze
  - Innerstädtische Zuzüge
  - Innerstädtische Wegzüge
  - Umzüge innerhalb des Gebietes
  - Wanderungssaldo

### Bildung
- Allgemeinbildende Schulen
  - Schulen insgesamt
  - Schüler insgesamt
  - Grundschulen
  - Schüler an Grundschulen
  - Mittel-/Oberschulen
  - Schüler an Mittel-/Oberschulen
  - Gymnasien
  - Schüler an Gymnasien
  - Förderschulen
  - Schüler an Förderschulen

### Erwerbstätigkeit und Arbeitsmarkt
- Arbeitslose
  - Arbeitslose insgesamt
  - Männer
  - Frauen
  - unter 25 Jahre
  - über 55 Jahre
  - Ausländer
  - Langzeitarbeitslose
  - Anteil an den Erwerbsfähigen
  - nach SGB III
  - nach SGB II
- Beschäftigte
  - Beschäftigte insgesamt
  - Männer
  - Frauen
  - unter 20 Jahre
  - 20 bis unter 25 Jahre
  - Ausländer
  - Anteil an den Erwerbsfähigen

### Gesundheit und Soziales
- Apotheken
  - Apotheken
- Ärzte
  - Ärzte
- Grundsicherung für Arbeitssuchende (SGB   II)
  - Bedarfsgemeinschaften
  - mit 1 Person
  - mit Kindern
  - Personen in Bedarfsgemeinschaften
  - SGB-II-Quote
  - Leistungsberechtigte
  - Erwerbsfähige Leistungsberechtigte
  - Anteil an den Erwerbsfähigen
  - Männer
  - Frauen
  - unter 25 Jahre
  - 25 bis unter 55 ahre
  - 55 Jahre und älter
  - Alleinerziehende
  - Ausländer
  - Nichterwerbsfähige
  - unter 15 Jahre
  - Anteil an Kindern
  - Nicht Leistungsberechtigte
- Kinder in Kindertagesstätten
  - Kinder insgesamt
  - Kinder bis unter 3 Jahre
  - Kinder 3 bis unter 6 Jahre
  - Kinder 6 Jahre und älter
  - Kinder ohne Migrationshintergrund
  - Kinder mit Migrationshintergrund
  - Schulkinder

### Lage und Territorium
- Flächennutzung
  - Gesamtfläche
  - Gebäude- und Freifläche
  - Verkehrsfläche
  - Landwirtschaftsfläche
  - Erholungsfläche
  - Waldfläche
  - Wasserfläche
  - Wohnbaufläche
  - Industrie- und Gewerbefläche

### Öffentliche Ordnung und Infrastruktur
- Straftaten
  - Straftaten insgesamt
  - Diebstahl
  - Körperverletzung
  - Straftaten je Einwohner
  - Vermögensdelikte

### Verkehr
- Kraftfahrzeugbestand
  - Kraftfahrzeuge insgesamt
  - PKW
  - Privat-PKW
  - Privat-PKW je 1 000 Einwohner
  - Nutzfahrzeuge
  - Krafträder
  - Kraftfahrzeuganhänger

### Wahlen
- Bundestagswahlen
  - Wahlbeteiligung
  - Stimmenanteile CDU
  - Stimmenanteile DIE LINKE
  - Stimmenanteile SPD
  - Stimmenanteile FDP
  - Stimmenanteile GRÜNE
  - Stimmenanteile NPD
  - Stimmenanteile AfD
- Europawahlen
  - Wahlbeteiligung
  - Stimmenanteile CDU
  - Stimmenanteile DIE LINKE
  - Stimmenanteile SPD
  - Stimmenanteile FDP
  - Stimmenanteile GRÜNE
  - Stimmenanteile NPD
  - Stimmenanteile AfD
- Landtagswahlen
  - Wahlbeteiligung
  - Stimmenanteile CDU
  - Stimmenanteile DIE LINKE
  - Stimmenanteile SPD
  - Stimmenanteile GRÜNE
  - Stimmenanteile FDP
  - Stimmenanteile Sonstige
  - Stimmenanteile NPD
  - Stimmenanteile AfD

### Wirtschaft
- Einzelhandel
  - Unternehmen insgesamt
  - Nahrungs- und Genussmittel
  - Gesundheit und Körperpflege
  - Bau- und Gartenmarkt
  - Bücher
  - Bekleidung und Schuhe
  - Elektrowaren und Multimedia
  - Hausrat
  - Sonstiger Einzelhandel
  - Verkaufsfläche insgesamt
  - Freizeit- und Sportartikel
- Firmen (WZ 2008)
  - Firmen insgesamt
  - Land- und Forstwirtschaft
  - Verarbeitendes Gewerbe
  - Energieversorgung
  - Wasserversorgung
  - Baugewerbe
  - Handel
  - Verkehr und Lagerei
  - Gastgewerbe
  - Information und Kommunikation
  - Erbringung von Finanz- und Versicherungsdienstleistungen
  - Grundstücks- und Wohnungswesen
  - Erbringung von freiberuflichen
  - Erbringung von sonstigen wirtschaftlichen Dienstleistungen
  - Erziehung und Unterricht
  - Gesundheits- und Sozialwesen
  - Kunst
  - Erbringung von sonstigen öffentlichen und privaten Dienstleistungen
- Firmen (WZ 2003)
  - Firmen insgesamt
  - Land- und Forstwirtschaft
  - Verarbeitendes Gewerbe
  - Energie- und Wasserversorgung
  - Baugewerbe
  - Handel
  - Gastgewerbe
  - Verkehr und Nachrichtenübermittlung
  - Kredit- und Versicherungsgewerbe
  - Grundstücks- und Wohnungswesen
  - Erziehung und Unterricht
  - Gesundheits-, Veterinär- und Sozialwesen
  - Erbringung von sonstigen öffentlichen und persönlichen   Dienstleistungen
- Handwerksbetriebe
  - Handwerksbetriebe insgesamt
  - Bau- und Ausbaugewerbe
  - Metallgewerbe
  - Holzgewerbe
  - Bekleidungs-, Textil- und Ledergewerbe
  - Nahrungsmittelgewerbe
  - Gewerbe für Gesundheits- und Körperpflege sowie des chemischen und Reinigungsgewerbes
  - Glas-, Papier-, Keramik- und sonstige Gewerbe
  - Handwerksähnliche Betriebe