require("leipzig-wahlkreise.js");

function getWahlkreisForOrtsteil(ortsteil){

    var foundWahlkreis = "not-found";
    Object.keys(leipzig.wahlkreise).forEach(function(wahlkreis) {
        leipzig.wahlkreise[wahlkreis].Ortsteile.forEach(function(aOrtsteil){
            if ( aOrtsteil == ortsteil ) foundWahlkreis = wahlkreis;
        })
    });

    return foundWahlkreis;
}

module.exports = {
    leipzig,
    getWahlkreisForOrtsteil
}