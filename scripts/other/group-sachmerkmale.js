const fs = require('fs');
const csv = require("fast-csv");
const path = __dirname + "/../data/open-data-portal-leipzig/";

var data = {};

fs.readdir(path, function(err, files) {

    for (let file of files) {

        var stream = fs.createReadStream(path + file);

        var csvStream = csv({headers:true})
            .on("data", function(line){

                // now we need to add all remaing columns to the sachmerkmal
                const sachmerkmal = line.Sachmerkmal.trim();
                if ( typeof data[file] == 'undefined' ) {
                    data[file] = new Set();
                }

                data[file].add(sachmerkmal);
            })
            .on("end", function(){

                console.log("------------------------------------------------------------")
                Object.keys(data).forEach(function(filename) {
                    console.log("### " + filename.replace(".csv", "").replace("_", " "))
                    data[filename].forEach(function(element){
                        console.log("- " + element)
                    });
                });
            });

        stream.pipe(csvStream);
    }
});