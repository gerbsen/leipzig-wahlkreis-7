const fs = require('fs');
const csv = require("fast-csv");
const leipzig = require("../../data/leipzig-wahlkreise");
const path = __dirname + "../../data/open-data-portal-leipzig/";

console.log(__dirname)
console.log(path)

// var data = {};
// var notFoundWahlkreise = new Set();

// fs.readdir(path, function(err, files) {

//     for (let file of files) {

//         var stream = fs.createReadStream(path + file);

//         var csvStream = csv({headers:true})
//             .on("data", function(line){

//                 var superElectorate = leipzig.getWahlkreisForOrtsteil(line.Gebiet.trim());
//                 if (superElectorate == "not-found") notFoundWahlkreise.add(line.Gebiet.trim());

//                 // we have to create an entry for the electorate
//                 if (!(superElectorate in data))
//                     data[superElectorate] = {};

//                 // now we have to create a entry each "Sachmerkmal"
//                 // for each electorate
//                 const electorate = data[superElectorate];

//                 if (!(line.Sachmerkmal.trim() in electorate))
//                     electorate[line.Sachmerkmal.trim()] = {};

//                 // now we need to add all remaing columns to the sachmerkmal
//                 const sachmerkmal = electorate[line.Sachmerkmal.trim()]

//                 Object.keys(line).forEach(function(column) {
//                     if ( column != "Gebiet" && column != "Sachmerkmal" ) {
//                         // console.log();
//                         var increment = sachmerkmal[column] == undefined ? 0 :
//                             (isNaN(line[column].replace(",", ".")) ? 0 : Number(line[column].replace(",", ".")))

//                         sachmerkmal[column] =
//                             (sachmerkmal[column] == undefined ? 0 : sachmerkmal[column]) +
//                             increment;
//                     }
//                 });
//             })
//             .on("end", function(){

//                 // I have to write the file multiple times, since I don't know
//                 // how to do that any other way
//                 fs.writeFile(__dirname + "/../data/merged-data-wahlkreise.js", "var leipzigWahlkreise = " + JSON.stringify(data, null, 4) + ";", function(err) {
//                     if(err) {
//                         return console.log(err);
//                     }
//                     console.log("The file was saved!")
//                     // console.log(notFoundWahlkreise);
//                 });
//             });

//         stream.pipe(csvStream);
//     }
// });