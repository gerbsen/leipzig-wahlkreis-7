const fs = require('fs');
const _ = require('lodash');
const x = require(__dirname + "/../../data/merged-data-ortsteile.js")

leipzigOrtsteile.features.forEach(function(feature){

    feature.properties["Privat-PKW/Einwohner insgesamt"] = {};

    _.keys(feature.properties["Privat-PKW"]).forEach(function(key){

        feature.properties["Privat-PKW/Einwohner insgesamt"][key] =
            feature.properties["Privat-PKW"][key] / feature.properties["Einwohner insgesamt"][key]
    });

    console.log(feature.properties.name)
    console.log(feature.properties["Privat-PKW/Einwohner insgesamt"])
})

// I have to write the file multiple times, since I don't know
// how to do that any other way
fs.writeFile(__dirname + "/../../data/extended-data-ortsteile.js", "var leipzigOrtsteile = " + JSON.stringify(leipzigOrtsteile, null, 4) + ";", function(err) {
    if(err) {
        return console.log(err);
    }
    console.log("The file was saved!");
});