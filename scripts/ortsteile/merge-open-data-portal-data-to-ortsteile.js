const fs = require('fs');
const csv = require("fast-csv");
const _ = require("lodash");
const leipzigGeoJson = require("../../data/geojson/leipzig.js");
const path = __dirname + "/../../data/open-data-portal-leipzig/";

var data = {};

fs.readdir(path, function(err, files) {

    for (let file of files) {

        var stream = fs.createReadStream(path + file);

        var csvStream = csv({headers:true})
            .on("data", function(line){

                // we have to create an entry for the electorate
                if (!(line.Gebiet.trim() in data))
                    data[line.Gebiet.trim()] = {};

                // now we have to create a entry each "Sachmerkmal"
                // for each electorate
                const electorate = data[line.Gebiet.trim()];

                if (!(line.Sachmerkmal.trim() in electorate))
                    electorate[line.Sachmerkmal.trim()] = {};

                // now we need to add all remaing columns to the sachmerkmal
                const sachmerkmal = electorate[line.Sachmerkmal.trim()]

                Object.keys(line).forEach(function(column) {
                    if ( column != "Gebiet" && column != "Sachmerkmal" ) {
                        sachmerkmal[column] = Number(line[column].replace(",", "."));
                    }
                });
            })
            .on("end", function(){

                _.keys(data).forEach(function(electorateName){
                    leipzigGeoJson.features.forEach(function(feature){
                        if ( feature.properties.name == electorateName ) {
                            Object.assign(feature.properties, data[electorateName]);
                        }
                    })
                });

                // I have to write the file multiple times, since I don't know
                // how to do that any other way
                fs.writeFile(__dirname + "/../../data/merged-data-ortsteile.js", "var leipzigOrtsteile = " + JSON.stringify(leipzigGeoJson, null, 4) + ";", function(err) {
                    if(err) {
                        return console.log(err);
                    }
                    console.log("The file was saved!");
                });
            });

        stream.pipe(csvStream);
    }
});